//
//  AppDelegate.h
//  TestLayout
//
//  Created by shintanaka on 2015/11/30.
//  Copyright © 2015年 shintanaka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

