//
//  main.m
//  TestLayout
//
//  Created by shintanaka on 2015/11/30.
//  Copyright © 2015年 shintanaka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
